﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SongDb.DataModels
{
    public class Album : Entity
    {
        [Required, MaxLength(200)]
        public string Title { get; set; }

        public Artist Artist { get; set; }

        public ICollection<Song> Songs { get; set; }
    }
}
