﻿using System;
using System.ComponentModel.DataAnnotations;

namespace SongDb.DataModels
{
    public abstract class Entity
    {
        [Key]
        public int Id { get; set; }

        public DateTime SavedOn { get; set; }

        public DateTime ModifiedOn { get; set; }

        public bool IsDeleted { get; set; }

        public DateTime? DeletedOn { get; set; }
    }
}
