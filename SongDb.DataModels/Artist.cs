﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SongDb.DataModels
{
    public class Artist : Entity
    {
        [Required, MaxLength(200)]
        public string Name { get; set; }

        public ICollection<Album> Albums { get; set; }

        public ICollection<SongArtist> SongArtist { get; set; }
    }
}
