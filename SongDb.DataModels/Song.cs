﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace SongDb.DataModels
{
    public class Song : Entity
    {
        [Required, MaxLength(200)]
        public string Name { get; set; }

        public int Miliseconds { get; set; }

        public ICollection<SongArtist> SongArtist { get; set; }

        public Album Album { get; set; }

        public Genre Genre { get; set; }
    }
}
