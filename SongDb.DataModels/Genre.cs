﻿using System.ComponentModel.DataAnnotations;

namespace SongDb.DataModels
{
    public class Genre : Entity
    {
        [Required, MaxLength(200)]
        public string Name { get; set; }
    }
}
