﻿using System.Collections.Generic;

namespace SongDb.Dtos
{
    public class AlbumDto
    {
        public int? Id { get; set; }

        public string Title { get; set; }

        public ArtistDto Artist { get; set; }

        public ICollection<SongDto> Songs { get; set; }
    }
}
