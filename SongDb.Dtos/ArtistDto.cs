﻿using System.Collections.Generic;

namespace SongDb.Dtos
{
    public class ArtistDto
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public ICollection<AlbumDto> Albums { get; set; }

        public ICollection<SongDto> Songs { get; set; }
    }
}
