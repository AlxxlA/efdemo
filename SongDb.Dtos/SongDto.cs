﻿using System.Collections.Generic;

namespace SongDb.Dtos
{
    public class SongDto
    {
        public int? Id { get; set; }

        public string Name { get; set; }

        public int Miliseconds { get; set; }

        public ICollection<ArtistDto> Artists { get; set; }

        public AlbumDto Album { get; set; }

        public GenreDto Genre { get; set; }
    }
}
