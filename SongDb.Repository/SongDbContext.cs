﻿using Microsoft.EntityFrameworkCore;
using SongDb.Common;
using SongDb.DataModels;
using System.Linq;

namespace SongDb.Repository
{
    public class SongDbContext : DbContext
    {
        private readonly ITimeProvider timeProvider;

        public SongDbContext()
        {

        }

        public SongDbContext(ITimeProvider timeProvider)
        {
            this.timeProvider = timeProvider;
        }

        public DbSet<Artist> Artists { get; set; }
        public DbSet<Song> Songs { get; set; }
        public DbSet<Album> Albums { get; set; }
        public DbSet<Genre> Genres { get; set; }
        public DbSet<SongArtist> SongsArtists { get; set; }

        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            optionsBuilder.UseSqlServer(@"Server=.\SQLEXPRESS;Database=SongDB;Trusted_Connection=True;");
        }

        public override int SaveChanges()
        {
            this.ApplyAuditInfoRules();
            return base.SaveChanges();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            base.OnModelCreating(builder);

            builder.Entity<SongArtist>()
                .HasOne(sa => sa.Song)
                .WithMany(s => s.SongArtist)
                .HasForeignKey(sa => sa.SongId);

            builder.Entity<SongArtist>()
                .HasOne(sa => sa.Artist)
                .WithMany(a => a.SongArtist)
                .HasForeignKey(sa => sa.ArtistId);
        }

        private void ApplyAuditInfoRules()
        {
            var entries = this.ChangeTracker.Entries()
                .Where(e => e.Entity is Entity);

            foreach (var entry in entries)
            {
                var entity = (Entity)entry.Entity;

                if (entry.State == EntityState.Added)
                {
                    entity.SavedOn = timeProvider.Now;
                }

                entity.ModifiedOn = timeProvider.Now;
            }
        }
    }
}
