﻿using SongDb.Common;
using SongDb.DataModels;
using System;
using System.Linq;

namespace SongDb.Repository
{
    public class EntityFrameworkRepository : IRepository
    {
        private readonly SongDbContext context;
        private readonly ITimeProvider timeProvider;

        public EntityFrameworkRepository(SongDbContext context, ITimeProvider timeProvider)
        {
            this.context = context ?? throw new ArgumentNullException(nameof(context));
            this.timeProvider = timeProvider;
        }

        public void Add(Entity entity)
        {
            context.Add(entity);
        }

        public T GetById<T>(int id) where T : Entity
        {
            return context.Find<T>(id);
        }

        public IQueryable<T> GetQueryable<T>(bool includeDeleted = false) where T : Entity
        {
            if (includeDeleted)
            {
                return context.Set<T>();
            }

            return context.Set<T>().Where(x => !x.IsDeleted);
        }

        public void Delete(Entity entity)
        {
            entity.IsDeleted = true;
            entity.DeletedOn = timeProvider.Now;
        }

        public int Save()
        {
            return context.SaveChanges();
        }
    }
}
