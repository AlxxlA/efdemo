﻿using SongDb.DataModels;
using System.Linq;

namespace SongDb.Repository
{
    public interface IRepository
    {
        void Add(Entity entity);

        T GetById<T>(int id) where T : Entity;

        IQueryable<T> GetQueryable<T>(bool includeDeleted = false) where T : Entity;

        void Delete(Entity entity);

        int Save();
    }
}
