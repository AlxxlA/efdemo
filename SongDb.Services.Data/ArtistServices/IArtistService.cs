﻿using SongDb.Dtos;
using System.Collections.Generic;

namespace SongDb.Services.Data.ArtistServices
{
    public interface IArtistService
    {
        void AddArtist(ArtistDto artistDto);

        ArtistDto GetArtist(int id);

        IEnumerable<ArtistDto> GetAllArtists();

        void Update(ArtistDto artistDto);

        void DeleteArtist(int id);
    }
}
