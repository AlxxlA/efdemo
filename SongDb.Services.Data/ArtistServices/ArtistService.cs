﻿using SongDb.Common;
using SongDb.DataModels;
using SongDb.Dtos;
using SongDb.Repository;
using System;
using System.Collections.Generic;
using System.Linq;

namespace SongDb.Services.Data.ArtistServices
{
    public class ArtistService : IArtistService
    {
        private readonly IRepository repository;
        private readonly IMappingProvider mappingProvider;

        public ArtistService(IRepository repository, IMappingProvider mappingProvider)
        {
            this.repository = repository ?? throw new ArgumentNullException(nameof(repository));
            this.mappingProvider = mappingProvider;
        }

        public void AddArtist(ArtistDto artistDto)
        {
            var newArtists = new Artist
            {
                Name = artistDto.Name,
            };

            repository.Add(newArtists);
            repository.Save();
        }

        public ArtistDto GetArtist(int id)
        {
            var artist = repository.GetById<Artist>(id);

            return EntityToDto(artist);
        }

        public IEnumerable<ArtistDto> GetAllArtists()
        {
            var artists = repository.GetQueryable<Artist>().ToList();

            return artists.Select(a => EntityToDto(a));
        }

        public void Update(ArtistDto artistDto)
        {
            var artist = repository.GetById<Artist>(artistDto.Id.Value);

            artist.Name = artistDto.Name;

            repository.Save();
        }

        public void DeleteArtist(int id)
        {
            var artist = repository.GetById<Artist>(id);

            if (artist == null)
            {
                return;
            }

            repository.Delete(artist);
            repository.Save();
        }

        private ArtistDto EntityToDto(Artist artist)
        {
            if (artist == null)
            {
                return null;
            }

            return new ArtistDto
            {
                Id = artist.Id,
                Name = artist.Name,
            };
        }
    }
}
