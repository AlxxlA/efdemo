﻿using System;

namespace SongDb.Common
{
    public class TimeProvider : ITimeProvider
    {
        public DateTime Now => DateTime.Now;

        public DateTime Today => DateTime.Today;
    }
}
