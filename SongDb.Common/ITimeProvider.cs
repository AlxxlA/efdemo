﻿using System;

namespace SongDb.Common
{
    public interface ITimeProvider
    {
        DateTime Now { get; }

        DateTime Today { get; }
    }
}
