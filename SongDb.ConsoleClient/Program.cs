﻿using SongDb.Common;
using SongDb.Dtos;
using SongDb.Repository;
using SongDb.Services.Data.ArtistServices;
using System;

namespace SongDb.ConsoleClient
{
    class Program
    {
        static void Main(string[] args)
        {
            var timeProvider = new TimeProvider();

            using (var dbContext = new SongDbContext(timeProvider))
            {
                var command = Console.ReadLine();

                var artistService = new ArtistService(new EntityFrameworkRepository(dbContext, timeProvider), null);

                while (command != "exit")
                {
                    if (command == "add")
                    {
                        var name = Console.ReadLine();

                        var artistDto = new ArtistDto { Name = name };
                        artistService.AddArtist(artistDto);
                        Console.WriteLine($"Artist {name} added");
                    }
                    else if (command == "get")
                    {
                        var id = int.Parse(Console.ReadLine());

                        var artist = artistService.GetArtist(id);

                        if (artist != null)
                        {
                            Console.WriteLine($"{artist.Id}. {artist.Name}");
                        }
                    }
                    else if (command == "get all")
                    {
                        var artists = artistService.GetAllArtists();

                        foreach (var artist in artists)
                        {
                            Console.WriteLine($"{artist.Id}. {artist.Name}");
                        }
                    }
                    else if (command == "update")
                    {
                        var id = int.Parse(Console.ReadLine());
                        var name = Console.ReadLine();

                        var artist = artistService.GetArtist(id);

                        artist.Name = name;

                        artistService.Update(artist);

                        Console.WriteLine($"Artist {id} is updated");
                    }
                    else if (command == "delete")
                    {
                        var id = int.Parse(Console.ReadLine());

                        artistService.DeleteArtist(id);

                        Console.WriteLine($"Artist {id} deleted");
                    }

                    command = Console.ReadLine();
                }
            }
        }
    }
}
